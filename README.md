##Bzzy: The Universal Status

Bzzy is a platform to provide users a quick and easy method of alerting friends and followers of one's current status and location, with a multitude of different options to protect privacy. 

As such, Bzzy is a "universal status" - at the end of the MVP, the application will have the capability of cross-posting statuses to multiple social networks if the user desires. As such, it will also function as a basic CRM solution for individuals - a  "CRM for the rest of us". 

The application comes in multiple parts. 

| Part             | Platform                 | Stage                  |
| -------------    |:-------------            | :-----                 |
| Web App          | Node.js (MEAN)           | Mid-Late Development   |
| Android App      | Java/Android SDK         | Early Development      |
| Glass App        | Node.js using Mirror SDK | Prototyping and Design |

This is a project that is still in frenzied development - it is a project I truly feel passionate about. As such, while it is not at the Minimum Viable Product stage *yet*, I'm anticipating rapid growth in the codebase over the next several weeks. 

Of course, the name is subject to domain availablility - it might not end up being bzzy. 


###Goals
In order to be classified as an MVP, the application will meet the following requirements. (**Bolded** entries are currently being worked on)

* Fully featured Web App
    * Ability to create statuses with time and location stamps
    * Ability to view prior statuses
    * **Ability to search for other bzzy users**
    * Ability to cross post to at least Twitter and **Facebook**
* Fully featured RESTful API including..
    * Ability to create, update, delete statuses
    * **Ability to create meetups based on proximity**
* **Android application featuring..**
    * Ability to create meetups based on proximity 
    * Ability to create statuses
    * Ability to view statuses of friends/followers 
* **Glass Application featuring..**
    * Ability to quickly create statuses through voice dictation
    * Ability to view the statuses of friends and followers


###Notes 

* For ideal Twitter profile image size, remove the _bigger appendage at the file name. 

