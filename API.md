#Internal API Documentation

###Things to Remember

* Express bodyparser ONLY accepts content that is application/json. You have been warned! 

* For testing, use the following OAuth Key: ``98676347-n7D2WAblB55Zv4xomGtBMC3szNC5Oj8MDBph8JDX5`` (Foxh8er)

Current Endpoints: 

* **GET** ``/api/{{key}}/user/getUser``
    
    Returns status and user object if successful. 
    
                {
                  "status": 200,
                  "user": {
                    "username": "USERNAME",
                    "name": "John Doe",
                    "followers": [
                      "barackobama", 
                      "housespeaker"
                    ],
                    "following": [],
                    "icon": "https://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png"
                  }
                }


    **Deprecated. User model is now filtered.**

    Deprecated Example JSON: 

            {
                "status": 200,
                "user": {
                    "__v": 0,
                    "_id": "",
                    "oauth_key": "",
                    "username": "TWITTER_USERNAME",
                    "past_meetups": [],
                    "pending_meetups": [],
                    "isFollowing": [],
                    "followers": [],
                    "statuses": [
                    {
                        "location": {
                            "longitude": "", 
                            "lattitude":"", 
                        },
                        "_id": "",
                        "quickstatus": "Available",
                        "status": "Status One.",
                        "date": "2013-12-03T22:28:39.873Z"
                    },
                    ],
                    "profile_data": {
                        "oauth_key": "TWITTER_OAUTH_KEY",
                        "name": "TWITTER_REALNAME",
                        "email": [],
                        "icon": [{"value": ""}]
                    }
                }
            }

* **GET** ``/api/{{key}}/user/follow/{{username}}``
    
    Returns confirmation JSON object. 

    Example JSON: 

            {
                "status": 200,
                "isFollowing": false
            }

* **GET** ``/api/:key/user/search/:user ``

    Get user object for *another* user. Used in query system. Currently its 1-1, but considering "instant search"

    Example JSON is identical to the user JSON example. 

* **POST** ``/api/{{key}}/user/follow/{{username}}``

    Posted data must conform to example POST JSON object. 

            {
                "status": "First Status",
                "location": {
                    //Obtained via geolocation. Placeholders required. 
                    "longitude": "", 
                    "lattitude": "", 
                }, 
                //Part of the enum
                "quickstatus": "", 
            }

    Returned data will be in the following form if successful. 

            {
                "status": 200, 
                "new_status": {
                    //Received status here
                }
            }

    If unsuccessful...

            {
                "status": 400, 
                "message": "{{Error Message}}"
            }