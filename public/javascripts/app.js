function AppCtrl($scope, $http, $window) {
    //Instantiate positioning object
    var pos = null;

    //Determine if browser is HTML5/geolocation compliant. 
    if (navigator.geolocation) {
        //pass in #getPosition() 
        navigator.geolocation.getCurrentPosition(getPosition);
    }

    //Array of possible options for quickstatuses. 
    $scope.quickstatuses = ["Available", "Busy", "Eating", "Gaming", "Sleeping", "Working", "Away"];
    //Set default selected as the first option
    $scope.selected = $scope.quickstatuses[0];

    //Post status to server via Ajax. 
    $scope.post = function() {
        if ($scope.status == undefined || $scope.status == "") {
            //Warn user if no status has been defined. 
            $scope.alertWarnShow = "true";
            $scope.alert = "Whoa there cowboy! You don't have a status!"
        } else {

            var status = null
            if (pos == null) {
                status = {
                    status: $scope.status,
                    location: {
                        longitude: "", 
                        lattitude: "", 
                    }, 
                    quickstatus: $scope.selected,
                };
            } else {
                status = {
                    status: $scope.status,
                    location: {
                        longitude: pos.coords.longitude,
                        lattitude: pos.coords.latitude,
                    },
                    quickstatus: $scope.selected,
                };
            }

            //Post to server. 
            $http.post("/app/status/new", status)
                .success(function(data, status, headers, config) {
                    //Refresh page if successful
                    //TODO: Make completely asynchronous. Maybe websockets?
                    $window.location.reload();
                }).error(function(data, status, headers, config) {
                    //alert user if Error
                    $scope.alertFailShow = "true";
                    $scope.alert = "Something's fishy. Try again. "
                });
        }
    };

    //Get randomized placeholder and greeting text on DOM load
    function placeholder() {
        var placeholders = ["What's up?", "How's it hanging?", "What are you up to?", "What are your plans this evening?", "Whatcha doing?"];
        var max = placeholders.length;
        var min = 0;
        $scope.placeholder = placeholders[Math.floor(Math.random() * max) + min];
    };

    function greet() {
        var greetings = ["Hi diddly ho", "Hey howdy hey", "Welcome back", "¡Hola", "Hey"];
        var max = greetings.length;
        var min = 0;
        $scope.greeting = greetings[Math.floor(Math.random() * max) + min];
    };

    //Sets position data from the navigator. Is there any better way?

    function getPosition(position) {
        pos = position;
    };

    //Set placeholder and greeting
    greet();
    placeholder();
};
