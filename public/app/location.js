function LocationCtrl($scope) {
    //Global Map Variable
    var map;

    var maps = {
        initializeGoogleMaps: function() {
            var mapOptions = {
                center: new google.maps.LatLng(40, -100),
                zoom: 4
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            this.getPosition();
        },

        getPosition: function() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(pos) {
                    map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                    map.setZoom(8);
                });
            }
            this.setMarkers();
        },

        setMarkers: function() {
            //Have to reorg location data
            //Misspelled "lattiude"
            //INstead of "", do undefined
            if (followerUsers.length > 0) {
                followerUsers.forEach(function(user) {
                    if (user.location) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(user.location.latitude, user.location.longitude),
                            title: user.username,
                            image: user.picture,
                        });

                        marker.setMap(map);
                    }
                });
            }

            if (followingUsers.length > 0) {
                followingUsers.forEach(function(user) {
                    if (user.location) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(user.location.latitude, user.location.longitude),
                            title: user.username,
                            image: user.picture,
                        });
                        marker.setMap(map);
                    }
                });
            }
        },

    };


    maps.initializeGoogleMaps();


}
