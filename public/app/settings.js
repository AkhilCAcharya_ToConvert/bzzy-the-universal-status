function SettingsCtrl($scope, $http, $timeout) {

    $scope.locationSettings = ["Visible to All", "Visible to Friends", "Visible to Friends and Followers", "Visible to None"];
    $scope.statusSettings = ["Visible to All", "Visible to Friends", "Visible to Friends and Followers"];

    if (currentSettings.locationSettings && currentSettings.statusSettings) {
        //Location Settings
        if (currentSettings.locationSettings.shareToAll){
            $scope.selectedLocation = $scope.locationSettings[0];
        }

        if (currentSettings.locationSettings.shareToFriends){
			$scope.selectedLocation = $scope.locationSettings[1];
    	}

		if (currentSettings.locationSettings.shareToFriendsAndFollowers){
			$scope.selectedLocation = $scope.locationSettings[2];
		}

	    if (currentSettings.locationSettings.shareToNone){
			$scope.selectedLocation = $scope.locationSettings[3];
	    }

		//Status settings
		if (currentSettings.statusSettings.shareToAll){
		    $scope.selectedStatus = $scope.statusSettings[0];
		}

		if (currentSettings.statusSettings.shareToFriends){
		    $scope.selectedStatus = $scope.statusSettings[1];
		}

		if (currentSettings.statusSettings.shareToFriendsAndFollowers){
		    $scope.selectedStatus = $scope.statusSettings[2];
		}
	}else{
	    $scope.selectedLocation = $scope.locationSettings[0];
	    $scope.selectedStatus = $scope.statusSettings[0];
	}

var alerter = function(status, message) {
    switch (status) {
        case "success":
            $scope.alertSuccessShow = "true";
            break;
        case "warn":
            $scope.alertWarnShow = "true";
            break;
        case "fail":
            $scope.alertFailShow = "true";
            break;
    }

    $scope.alert = message;
    $timeout(function() {
        $scope.alertSuccessShow = "false";
        $scope.alertWarnShow = "false";
        $scope.alertFailShow = "false";
    }, 1500);
}

$scope.post = function() {

    var settings = {
        locationSettings: {
            shareToAll: $scope.selectedLocation == $scope.locationSettings[0],
            shareToFriends: $scope.selectedLocation == $scope.locationSettings[1],
            shareToFriendsAndFollowers: $scope.selectedLocation == $scope.locationSettings[2],
            shareToNone: $scope.selectedLocation == $scope.locationSettings[3],
        },
        statusSettings: {
            shareToAll: $scope.selectedStatus == $scope.statusSettings[0],
            shareToFriends: $scope.selectedStatus == $scope.statusSettings[1],
            shareToFriendsAndFollowers: $scope.selectedStatus == $scope.statusSettings[2],
        },
    };

  	if($scope.emailAddress != ""){
  		settings.email = $scope.emailAddress; 
  	}

    //Post to server. 
    $http.post("/app/settings", settings)
        .success(function(data, status, headers, config) {
            if (data.status == 200) {
                alerter("success", "Saved!");
            }
        }).error(function(data, status, headers, config) {
            alerter("fail", "Something's fishy. Try again");
        });
}


}
