function UserCtrl($scope, $http) {
    //Post data via AJAX when "follow/unfollow" button clicked
    $scope.post = function() {
        //Flip boolean on click 
        $scope.isFollowing = !$scope.isFollowing;

        //Instantiate follow object to POST
        var follow = {
            //Hackish way to get the user name. Must find a better way. 
            user: location.href.split('/')[4],
            isFollowing: $scope.isFollowing
        };

        $http.post('/app/user/follow', follow)
            .success(function(data, status, header, config) {
                //If successful, change the text (Color changes with boolean, I should fix that)
                $scope.following = $scope.isFollowing ? "Unfollow" : "Follow";
            }).error(function(data, status, header, config) {
                //Flip it back and pretend nothing happened. 
                $scope.isFollowing = !$scope.isFollowing;
            });

    };

    //on run
    function init() {
        //Set initial data
        $scope.isFollowing = isFollowing;
        $scope.following = $scope.isFollowing ? "Unfollow" : "Follow";
    };
    init();
};
