var Twitter = require('twit');
module.exports = function(config, token, tokenSecret) {
    var twit = new Twitter({
        consumer_key: config.get("twitter:consumer_key"),
        consumer_secret: config.get("twitter:secret_key"),
        access_token: token,
        access_token_secret: tokenSecret,
    });

    var object = {
        postStatus: function(status, quickstatus) {
            var promotion = "#bzzy";
            var quickstatus = "#" + quickstatus;
            var finalStatus = null;
            
            if (status.indexOf("#bzzy") != -1) {
                if ((status + " " + quickstatus + " " + promotion).length <= 140) {
                    finalStatus = status + " " + quickstatus + " " + promotion;
                } else if ((status + " " + promotion).length <= 140) {
                    finalStatus = status + " " + promotion;
                } else {
                    finalStatus = status;
                }
            } else {
                if ((status + " " + quickstatus).length <= 140) {
                    finalStatus = status + " " + quickstatus;
                } else {
                    finalStatus = status;
                }
            }

            var id = []; 
            var replyCallback = function(err, reply) {
                if (!err) {
                    id.push(reply.id_str);
                    console.log("Posted");
                } else {
                    console.log("Error: " + err);
                    console.log("Reply: " + reply);
                    id.push("");
                }
                id.join("");
            }
            twit.post('statuses/update', {status: finalStatus}, replyCallback(err, reply));
        }
    };
    return object;
}



/*

//*****CURRENTLY NOT USED*******
//Determine how to modularize this. 
passport.use(new TwitterStrategy({
    consumerKey: config.get("twitter:consumer_key"),
    consumerSecret: config.get('twitter:secret_key'),
    callbackURL: config.get('twitter:callback_url'),
}, function(token, tokenSecret, profile, done) {
    //Create User
    var name = profile.name;
    var User = mongoose.model('User');
    User.findOne({
        oauth_key: token
    }, function(err, user) {
        if (user) {
            console.log('Found user');
            done(null, user.oauth_key);
        } else {
            var newUser = require('./models/EmptyUser.js')(mongoose, token, profile);
            newUser.save(function(err) {
                if (!err) {
                    console.log("New User Saved");
                    done(null, newUser.oauth_key);
                } else {
                    throw err;
                }
            });
        }
    });
}));

*/
