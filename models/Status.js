var mongoose = require('mongoose');
var statusSchema = new mongoose.Schema({
    id: String, 
    date: Date,
    location: {
        longitude: String,
        latitude: String,
        location_str: String, 
    },
    status: String,
    quickstatus: String, 
});
mongoose.model('Status', statusSchema);
