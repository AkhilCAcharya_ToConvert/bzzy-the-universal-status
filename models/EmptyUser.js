module.exports = function(mongoose, token, tokenSecret, profile) {
    var User = mongoose.model('User'); 
    var Meetup = mongoose.model('Meetup'); 

    var meetups = []; 
    
    meetups.push(new Meetup({
        users: [], 
        meetup_name: "First meetup", 
        date: new Date(), 
        location:{
            longitude: "0.00", 
            lattitude: "0.00", 
        },
    }));     


    var newUser = new User({
        oauth_key: token,
        oauth_secret: tokenSecret, 
        username: profile.username.toLowerCase(),
        profile_data: {
            oauth_key: token,
            name: profile.displayName,
            icon: profile.photos,
            email: profile.emails,
        },
        statuses: [],
        followers: [],
        isFollowing: [], 
        pending_meetups: meetups,
        past_meetups: [],
    });

    return newUser;
}
