var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    oauth_key: String,
    oauth_secret: String, 
    username: String, 
    profile_data: {
        name: String,
        icon: Array,
    },
    settings: {
        statusSettings: {
            shareToFriends: Boolean,
            shareToFriendsAndFollowers: Boolean, 
            shareToAll: Boolean,  
        }, 
        locationSettings: {
            shareToFriends: Boolean,
            shareToFriendsAndFollowers: Boolean, 
            shareToAll: Boolean, 
            shareToNone: Boolean, 
        },  
        email: String, 
    }, 
    statuses: Array, //Status Objects
    followers: Array,
    isFollowing: Array,  //Friend Objects
    pending_meetups: Array, //Metup Objects
    past_meetups: Array, //Meetup Objects
});

mongoose.model("User", userSchema);
