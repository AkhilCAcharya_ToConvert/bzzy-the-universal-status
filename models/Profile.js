module.exports = function(Schema) {
    var profileSchema = new Schema({
    	oauth_key: String, 
        name: String,
        icon: Array,
        email: Array,
    });

    return profileSchema;  
}
