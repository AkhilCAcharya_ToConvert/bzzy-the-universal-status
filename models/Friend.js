module.exports = function(mongoose) {

    var friendSchema = new mongoose.Schema({
        name: String,
        profile: [],
        meetups: [],
        isSpeedDial: Boolean,
    });

    return mongoose.model('Friend', meetupSchema);
}
