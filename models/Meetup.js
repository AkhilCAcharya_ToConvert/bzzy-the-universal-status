var mongoose = require('mongoose');
var meetupSchema = new mongoose.Schema({
    users: [], //Profile Schemas
    meetup_name: String,
    date: Date,
    location: {
        longitude: String,
        lattitude: String,
    }
});

mongoose.model('Meetup', meetupSchema);
