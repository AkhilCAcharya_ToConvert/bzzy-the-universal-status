/*****************************
 * Author: Akhil Acharya
 * Project: Bzzy, the Universal Status
 * Started: 11/22/13
 ******************************/
var express = require('express');

//Configuration Data
var config = require(__dirname + "/config.json");

//Auth
var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;

//DB
var mongoose = require('mongoose');
var db_url = config.db.base_url + config.db.username + ":" + config.db.password + config.db.path;

mongoose.connect(db_url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));

db.once('open', function callback() {
    console.log("Connected to remote MongoDB");
})

//App
var app = module.exports = express.createServer();

//Email
//var sendgrid = require('sendgrid')(config.sendgrid.api_user, config.sendgrid.api_key);


//Models
require('./models/UserModel.js'); //Instantiate User 
require('./models/Meetup.js'); //Instantiate Meetup
require('./models/Status.js'); //Instantiate Status
var User = mongoose.model('User'); //Call the model

//Controllers
var IndexController = require('./controllers/IndexController.js');
var APIController = require('./controllers/APIController.js')(config, mongoose, passport);
var WebController = require('./controllers/WebController.js')(config, mongoose, passport);

//Templating
var jade = require('jade');


app.configure(function() {
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({
        secret: "I'm actually Sri Lankan"
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(app.router);
    //app.use(express.methodOverride()); //Usually done to simulate PUT and DELETE //--> Somehow causes a an error with AJAX. Will investigate
    app.use(express.static(__dirname + '/public'));
});

// Configuration
passport.serializeUser(function(key, done) {
    done(null, key);
});

passport.deserializeUser(function(key, done) {
    //Drop in Persistence solution 
    User.findOne({
        oauth_key: key
    }, function(err, user) {
        done(err, user);
    });
});

//Determine how to modularize this. 
passport.use(new TwitterStrategy({
    consumerKey: config.twitter.consumer_key,
    consumerSecret: config.twitter.secret_key,
    callbackURL: config.twitter.callback_url,
}, function(token, tokenSecret, profile, done) {
    //Create User
    var name = profile.name;
    var User = mongoose.model('User');
    User.findOne({
        oauth_key: token
    }, function(err, user) {
        if (user) {
            console.log('Found user');
            done(null, user.oauth_key);
        } else {
            var newUser = require('./models/EmptyUser.js')(mongoose, token, tokenSecret, profile);
            newUser.save(function(err) {
                if (!err) {
                    console.log("New User Saved");
                    done(null, newUser.oauth_key);
                } else {
                    throw err;
                }
            });
        }
    });
}));


app.configure('development', function() {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});

//Landing Routes
app.get('/', IndexController.index);

/** API Routes **/
//General form: /api/::oauthkey/user/method
//:key ==> twitter oauth public key
//User: User name


//Get user object (used for refreshing)
app.get('/api/:key/user/get', isAPIAuthenticated, APIController.getUser);
//Search for user that isn't logged in user
app.get('/api/:key/user/search/:user', APIController.searchUser);

//Follow user. 
app.get('/api/:key/user/follow/:user', isAPIAuthenticated, APIController.followUser);

//Get All statuses
app.get('/api/:key/user/status/get', isAPIAuthenticated, APIController.getStatus);

//Create new status and append it to user object (the SOAP way)
app.post('/api/:key/user/status/new', APIController.newStatus);

//Delete Status (untested)
app.post('/api/:key/user/status/:id/delete', APIController.deleteStatus);


/** Web routes **/
app.get('/app', isWebAuthenticated, WebController.app);
app.get('/logout', WebController.logout);
app.get('/api/provisioning', WebController.provisioning);
app.get('/u/:id', WebController.user);
app.get('/location', isWebAuthenticated, WebController.location);
app.get('/settings', isWebAuthenticated, WebController.settings);

//Utility Web Routes
//Statuses
//Create new status
app.post('/app/status/new', WebController.newStatus);

//Delete status 
app.get('/app/status/delete/:id', isWebAuthenticated, WebController.deleteStatus);

//Follow or unfollow user
app.post('/app/user/follow', isWebAuthenticated, WebController.followUser);

//Save settings
app.post('/app/settings', isWebAuthenticated, WebController.settingsPost);


//Auth
app.get('/auth/twitter', WebController.login);
app.get('/auth/twitter/callback', WebController.callback);
app.get('/u/:id', WebController.user);



//Listen in
app.listen(8081, function() {
    if (!config.production) {
        console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
        console.log("Currently in testing mode");
    }
});

//Web Auth Middleware

function isWebAuthenticated(req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/');
    }
}

//API Key Auth middleware
var isAPIAuthenticated = function(req, res, next) {
    //Add provisioning checking here
    next();
}
