module.exports = function(config, mongoose, passport) {
    var moment = require('moment');
    var User = mongoose.model('User');
    var Status = mongoose.model('Status');
    var Twitter = require('twit');
    var geocoder = require('geocoder');
    var _ = require('lodash');

    var routes = {
        login: passport.authenticate('twitter', {
            session: true
        }),
        callback: passport.authenticate('twitter', {
            successRedirect: "/app",
            failureRedirect: "/"
        }),
        app: function(req, res) {

            //Get statuses for followed users. 
            //This might get query intensive...
            var following = req.user.isFollowing;
            var followers = req.user.followers;

            var followingUsers = [];
            var followerUsers = [];

            var renderUser = function() {
                console.log(followingUsers);
                res.render('applayout', {
                    user: req.user,
                    followingUsers: followingUsers,
                    followerUsers: followerUsers,
                    moment: moment
                });
            }

            var finished = _.after(following.length + followers.length, renderUser);

            //call original Query Function
            if (followers.length > 0 || following.length > 0) {
                following.forEach(function(followee) {
                    console.log(followee);
                    User.findOne({
                        username: followee
                    }, function(err, user) {
                        var queriedStatus = {};
                        if (user.statuses.length > 0) {
                            //get last status
                            var status = user.statuses[user.statuses.length - 1];
                            queriedStatus.status = status.status;
                            queriedStatus.quickstatus = status.quickstatus;
                            queriedStatus.date = status.date;
                            queriedStatus.location_str = status.location.location_str;
                        } else {
                            queriedStatus.status = "";
                            queriedStatus.quickstatus = "";
                            queriedStatus.date = "";
                            queriedStatus.location_str = "";
                        }
                        queriedStatus.username = followee;
                        queriedStatus.name = user.profile_data.name;
                        queriedStatus.picture = user.profile_data.icon[0].value;
                        followingUsers.push(queriedStatus);
                        finished();
                    });
                });

                followers.forEach(function(follower) {
                    console.log(follower);
                    User.findOne({
                        username: follower
                    }, function(err, user) {
                        var queriedStatus = {};
                        if (user.statuses.length > 0) {
                            //get last status
                            var status = user.statuses[user.statuses.length - 1];
                            queriedStatus.status = status.status;
                            queriedStatus.quickstatus = status.quickstatus;
                            queriedStatus.date = status.date;
                            queriedStatus.location_str = status.location.location_str;
                        } else {
                            queriedStatus.status = "";
                            queriedStatus.quickstatus = "";
                            queriedStatus.date = "";
                            queriedStatus.location_str = "";
                        }
                        queriedStatus.username = follower;
                        queriedStatus.name = user.profile_data.name;
                        queriedStatus.picture = user.profile_data.icon[0].value;
                        followerUsers.push(queriedStatus);
                        finished();
                    });
                });
            } else {
                res.render('applayout', {
                    user: req.user,
                    followingUsers: followingUsers,
                    followerUsers: followerUsers,
                    moment: moment
                });
            }
        },
        user: function(req, res) {
            User.findOne({
                username: req.params.id
            }, function(err, user) {
                if (user) {
                    if (req.user) {
                        res.render('user', {
                            foundUser: user,
                            moment: moment,
                            user: req.user
                        });
                    } else {
                        res.render('user', {
                            foundUser: user,
                            moment: moment
                        });
                    }
                } else {
                    res.send(404);
                }
            });
        },

        location: function(req, res) {

            var following = req.user.isFollowing;
            var followers = req.user.followers;

            var followingUsers = [];
            var followerUsers = [];

            var renderUser = function() {
                console.log(followingUsers);
                console.log(followerUsers); 
                res.render('location', {
                    user: req.user,
                    followingUsers: followingUsers,
                    followerUsers: followerUsers,
                    moment: moment
                });
            }

            var finished = _.after(following.length + followers.length, renderUser);

            //call original Query Function
            if (followers.length > 0 || following.length > 0) {
                following.forEach(function(followee) {
                    console.log(followee);
                    User.findOne({
                        username: followee
                    }, function(err, user) {
                        var queriedStatus = {};
                        if (user.statuses.length > 0) {
                            //get last status
                            var status = user.statuses[user.statuses.length - 1];
                            queriedStatus = {
                                status: status.status,
                                quickstatus: status.quickstatus,
                                date: status.date,
                                location: status.location,
                            };
                        } 
                        queriedStatus.username = followee;
                        queriedStatus.name = user.profile_data.name;
                        queriedStatus.picture = user.profile_data.icon[0].value;
                        followingUsers.push(queriedStatus);
                        finished();
                    });
                });

                followers.forEach(function(follower) {
                    console.log(follower);
                    User.findOne({
                        username: follower
                    }, function(err, user) {
                        var queriedStatus = {};
                        if (user.statuses.length > 0) {
                            //get last status
                            var status = user.statuses[user.statuses.length - 1];
                            queriedStatus = {
                                status: status.status,
                                quickstatus: status.quickstatus,
                                date: status.date,
                                location: status.location,
                            };
                        } 

                        queriedStatus.username = follower;
                        queriedStatus.name = user.profile_data.name;
                        queriedStatus.picture = user.profile_data.icon[0].value;
                        followerUsers.push(queriedStatus);
                        finished();
                    });
                });
            } else {
                res.render('location', {
                    user: req.user,
                    moment: moment
                });
            }
        },

        settingsPost: function(req, res) {
            var settings = req.body;
            console.log(settings);
            User.findOne({
                oauth_key: req.user.oauth_key
            }, function(err, user) {
                if (user) {
                    user.settings.locationSettings = settings.locationSettings;
                    user.settings.statusSettings = settings.statusSettings;

                    if (settings.email) {
                        user.settings.email = settings.email;
                    }

                    console.log("Saved Settings");
                    user.save(function(err) {
                        if (!err) {
                            res.send({
                                status: 200
                            });
                        } else {
                            res.send({
                                status: 401
                            });
                        }
                    })

                } else {
                    res.send({
                        status: 401
                    });
                }
            });
        },

        settings: function(req, res) {
            res.render('settings', {
                user: req.user
            });
        },

        followUser: function(req, res) {
            var follow = req.body;
            //Add to followers
            User.findOne({
                username: follow.user
            }, function(err, user) {
                if (user) {
                    var currentFollowers = user.followers;
                    if (currentFollowers.indexOf(req.user.username) == -1) {
                        currentFollowers.push(req.user.username);
                    } else {
                        currentFollowers.pop(req.user.username);
                    }
                    user.followers = currentFollowers;
                    user.save(function(err) {
                        if (err) {
                            res.send({
                                status: 401
                            });
                        } else {
                            res.send({
                                status: 200
                            });
                        }
                    })

                } else {
                    res.send({
                        status: 400
                    })
                }
            });

            //Add to isFollowing
            User.findOne({
                username: req.user.username
            }, function(err, user) {
                if (user) {
                    var isCurrentlyFollowing = user.isFollowing;
                    if (isCurrentlyFollowing.indexOf(follow.user) == -1) {
                        isCurrentlyFollowing.push(follow.user);
                    } else {
                        isCurrentlyFollowing.pop(follow.user);
                    }

                    user.isFollowing = isCurrentlyFollowing;
                    user.save(function(err) {
                        if (err) {
                            throw err;
                        } else {
                            console.log("Updated user");
                        }
                    })
                }
            })

        },
        newStatus: function(req, res) {
            //AJAX here
            var postedStatus = req.body;
            var token = req.user.oauth_key;
            var tokenSecret = req.user.oauth_secret;

            var twit = new Twitter({
                consumer_key: config.twitter.consumer_key,
                consumer_secret: config.twitter.secret_key,
                access_token: token,
                access_token_secret: tokenSecret,
            });

            //status items
            var location_str;
            var id;

            //After finishing, it saves the status. 
            var saveStatus = function() {
                User.findOne({
                    oauth_key: req.user.oauth_key
                }, function(err, user) {
                    if (err) {
                        throw err;
                        res.send({
                            status: 401
                        });
                    } else {
                        var newStatus = new Status({
                            date: new Date(),
                            id: id,
                            location: {
                                longitude: postedStatus.location.longitude,
                                latitude: postedStatus.location.latitude,
                                location_str: location_str,
                            },
                            status: postedStatus.status,
                            quickstatus: postedStatus.quickstatus,
                        });
                        var currentStatuses = user.statuses;
                        currentStatuses.push(newStatus);
                        user.statuses = currentStatuses;
                        user.save(function(err) {
                            if (err) {
                                throw err;
                                res.send({
                                    status: 400
                                });
                            }
                            res.send({
                                status: 201,
                                new_status: newStatus
                            });
                        });
                    }
                });
            }

            var finished = _.after(2, saveStatus);

            //Twitter Stuff
            var getStatus = function() {
                var promotion = "#bzzy";
                var status = postedStatus.status;
                var quickstatus = "#" + postedStatus.quickstatus;
                var finalStatus = null;
                if ((status + " " + quickstatus).length <= 140) {
                    return status + " " + quickstatus;
                } else {
                    return status;
                }
            }

            twit.post('statuses/update', {
                status: getStatus()
            }, function(err, reply) {
                if (!err) {
                    console.log("Posted");
                    id = reply.id_str;
                } else {
                    console.log("Error: " + err);
                    console.log("Reply: " + reply);
                }
                finished();
            });

            geocoder.reverseGeocode(postedStatus.location.lattitude, postedStatus.location.longitude, function(err, data) {
                if (err != undefined || data == undefined) {
                    finished();
                } else {
                    //City and State 
                    location_str = data.results[0].address_components[3].long_name + ", " + data.results[0].address_components[6].short_name;
                    finished();
                }
            });
        },

        deleteStatus: function(req, res) {
            var id = req.params.id;

            var token = req.user.oauth_key;
            var tokenSecret = req.user.oauth_secret;
            var twit = new Twitter({
                consumer_key: config.twitter.consumer_key,
                consumer_secret: config.twitter.secret_key,
                access_token: token,
                access_token_secret: tokenSecret,
            });

            User.findOne({
                oauth_key: req.user.oauth_key
            }, function(err, user) {
                var statuses = user.statuses;
                console.log("querying..");
                var queriedStatus = _.find(statuses, function(status) {
                    return status.id == id;
                });
                var saveStatus = function() {
                    if (queriedStatus) {
                        user.statuses.pop(queriedStatus);
                    }

                    //If the user has already deleted the status from Twitter, nothing else happens
                    user.save(function(err) {
                        if (err) {
                            throw err;
                        } else {
                            console.log("Saved!");
                            res.redirect("/u/" + req.user.username)
                        }
                    });
                }
                var finished = _.after(1, saveStatus);
                twit.post("statuses/destroy/:id", {
                    id: id
                }, function(err, reply) {
                    if (!err) {
                        finished();
                    } else {
                        finished();
                    }
                });
            });
        },

        logout: function(req, res) {
            req.logout();
            res.redirect('/');
        },
        provisioning: function(req, res) {
            res.render('provisioning', {
                user: req.user
            });
        },
    };
    return routes;
}
