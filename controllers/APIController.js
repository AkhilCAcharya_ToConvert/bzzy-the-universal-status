module.exports = function(config, mongoose, passport) {
    var User = mongoose.model('User');
    var utils = require('util');
    var Twitter = require('twit');
    var _ = require('lodash');
    var routes = {
        getUser: function(req, res) {
            var key = req.params.key;
            User.findOne({
                oauth_key: key
            }, function(err, user) {
                if (user == undefined || err != null) {
                    res.send({
                        status: 400,
                        error: utils.inspect(user)
                    });
                } else {
                    res.send({
                        status: 200,
                        user: getUser(user)
                    });
                }
            })
        },
        searchUser: function(req, res) {
            var key = req.params.key;
            var queryUsername = req.params.user;
            User.findOne({
                username: queryUsername
            }, function(err, user) {
                if (user == undefined) {
                    res.send({
                        status: 401
                    })
                } else if (err != undefined) {
                    res.send({
                        status: 400
                    })
                } else {
                    res.send({
                        status: 200,
                        user: getUser(user)
                    });
                }
            });
        },

        feed: function(req, res) {
            var key = req.params.id;
            User.findOne({
                oauth_key: key
            }, function(err, user) {
                if (user) {
                    var following = user.isFollowing;
                    var followers = user.followers;

                    //Return both
                    var followingUsers = [];
                    var followerUsers = [];  

                    var renderUser = function() {
                        console.log(followingUsers);
                        res.send({
                            status: 200, 
                            user: user.username,
                            followingUsers: followingUsers,
                            followerUsers: followerUsers,
                        });
                    }

                    var finished = _.after(following.length + followers.length, renderUser);

                    var queryUsers = function() {
                        following.forEach(function(followee) {
                            console.log(followee);
                            User.findOne({
                                username: followee
                            }, function(err, user) {
                                var queriedStatus = {};
                                if (user.statuses.length > 0) {
                                    //get last status
                                    var status = user.statuses[user.statuses.length - 1];
                                    queriedStatus.status = status.status;
                                    queriedStatus.quickstatus = status.quickstatus;
                                    queriedStatus.date = status.date;
                                    queriedStatus.location_str = status.location.location_str;
                                } else {
                                    queriedStatus.status = "";
                                    queriedStatus.quickstatus = "";
                                    queriedStatus.date = "";
                                    queriedStatus.location_str = "";
                                }
                                queriedStatus.username = followee;
                                queriedStatus.name = user.profile_data.name;
                                queriedStatus.picture = user.profile_data.icon[0].value;
                                followingUsers.push(queriedStatus);
                                finished();
                            });
                        });

                        followers.forEach(function(follower) {
                            console.log(follower);
                            User.findOne({
                                username: follower
                            }, function(err, user) {
                                var queriedStatus = {};
                                if (user.statuses.length > 0) {
                                    //get last status
                                    var status = user.statuses[user.statuses.length - 1];
                                    queriedStatus.status = status.status;
                                    queriedStatus.quickstatus = status.quickstatus;
                                    queriedStatus.date = status.date;
                                    queriedStatus.location_str = status.location.location_str;
                                } else {
                                    queriedStatus.status = "";
                                    queriedStatus.quickstatus = "";
                                    queriedStatus.date = "";
                                    queriedStatus.location_str = "";
                                }
                                queriedStatus.username = follower;
                                queriedStatus.name = user.profile_data.name;
                                queriedStatus.picture = user.profile_data.icon[0].value;
                                followerUsers.push(queriedStatus);
                                finished();
                            });
                        });
                    }
                    //call original Query Function
                    queryUsers();
                } else {
                    res.send({status:400, message: "No user"}); 
                }
            });
        },

        followUser: function(req, res) {
            var key = req.params.key;
            var followedUser = req.params.user;
            var followingUser = null;
            var message = "";
            var currentFollowers = null;
            var added = false;

            User.findOne({
                oauth_key: key
            }, function(err, user) {
                if (user) {
                    //Add to isFollowing 
                    if (user.username == followedUser) {
                        followingUser = "";
                        res.send({
                            status: 401,
                            message: "You can't follow yourself!"
                        });
                    } else {
                        followingUser = user.username;
                        var isCurrentlyFollowing = user.isFollowing;
                        if (isCurrentlyFollowing.indexOf(followedUser) == -1) {
                            isCurrentlyFollowing.push(followedUser);
                            added = true;
                        } else {
                            isCurrentlyFollowing.pop(followedUser);
                            added = false;
                        }
                        user.save(function(err) {
                            if (err) {
                                throw err;
                            } else {
                                User.findOne({
                                    username: followedUser
                                }, function(err, user) {
                                    if (user) {
                                        currentFollowers = user.followers;
                                        if (added) {
                                            currentFollowers.push(followingUser);
                                        } else {
                                            currentFollowers.pop(followingUser);
                                        }
                                        user.followers = currentFollowers;
                                        user.save(function(err) {
                                            if (!err) {

                                                res.send({
                                                    status: 200,
                                                    isFollowing: currentFollowers.indexOf(followingUser) != -1,
                                                });
                                                console.log("Updated user");

                                            }
                                        });
                                    }
                                })
                            };
                        });
                    }
                } else {
                    //Add clause
                }
            });
        },

        newStatus: function(req, res) {
            var postedStatus = req.body;
            var key = req.params.key;

            var newStatus = new Status({
                date: new Date(),
                location: {
                    longitude: postedStatus.location.longitude,
                    lattitude: postedStatus.location.lattitude,
                },
                status: postedStatus.status,
                quickstatus: postedStatus.quickstatus,
            });

            User.findOne({
                oauth_key: key
            }, function(err, user) {
                if (err) {
                    throw err;
                    res.send({
                        status: 401
                    });
                }
                var currentStatuses = user.statuses;
                currentStatuses.push(newStatus);
                user.statuses = currentStatuses;
                user.save(function(err) {
                    if (err) {
                        throw err;
                        res.send({
                            status: 400
                        });
                    }

                    //Post to Twitter
                    //var Twitter = require('../Strategies/Twitter.js')(config, req.user.oauth_key, req.user.oauth_secret); 
                    //Twitter.post(postedStatus.status); 

                    res.send({
                        status: 201,
                        new_status: newStatus
                    });
                })
            });
        },

        getStatus: function(req, res) {
            var key = req.params.key
            User.findOne({
                oauth_key: key
            }, function(err, user) {
                if (user) {
                    res.send({
                        status: 200,
                        statuses: user.statuses
                    });
                } else {
                    res.send({
                        status: 401,
                        message: "Error"
                    });
                }
            });
        },

        deleteStatus: function(req, res) {
            var key = req.params.key;
            var id = req.params.id;

            User.findOne({
                oauth_token: key
            }, function(err, user) {
                if (user) {
                    var twit = new Twitter({
                        consumer_key: config.get("twitter:consumer_key"),
                        consumer_secret: config.get("twitter:secret_key"),
                        access_token: user.oauth_key,
                        access_token_secret: user.oauth_secret,
                    });

                    var statuses = user.statuses;
                    console.log("querying..");
                    var queriedStatus = _.find(statuses, function(status) {
                        return status.id == id;
                    });
                    var saveStatus = function() {
                        if (queriedStatus) {
                            user.statuses.pop(queriedStatus);
                        }
                        //If the user has already deleted the status from Twitter, nothing else happens
                        user.save(function(err) {
                            if (err) {
                                throw err;
                            } else {
                                console.log("Saved!");
                                res.render({
                                    status: 200,
                                    message: "Deleted"
                                });
                            }
                        });
                    }
                    var finished = _.after(1, saveStatus);
                    twit.post("statuses/destroy/:id", {
                        id: id
                    }, function(err, reply) {
                        if (!err) {
                            finished();
                        } else {
                            finished();
                        }
                    });
                } else {
                    res.send({
                        status: 401,
                        message: "No User"
                    });
                }
            });
        },
    };

    var getUser = function(user) {
        return {
            username: user.username,
            name: user.profile_data.name,
            followers: user.followers,
            following: user.isFollowing,
            icon: user.profile_data.icon[0].value
        }
    };

    return routes;
};
